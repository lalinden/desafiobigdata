Desafio Engenheiro de dados por Leonardo Augusto Linden.
Executar com SPARK (spark-submit)


  Sao gerados 3 arquivos .txt:
  
  index.txt - indice desejado (word, ID)
  
  dict.txt - dicionario (ID, files)
  
  map_word_index.txt - mapa para verificacao/conferencia do word/ID/files
  

  Exemplo a palavra 'Fixedly' aparece como segue nos 3 arquivos:

  index.txt

  Fixedly                   14

  dict.txt

  (14, [11, 13, 15, 18, 2, 26, 30, 31, 34, 41, 9])

  map_word_index.txt

  Fixedly                   (14) - [11, 13, 15, 18, 2, 26, 30, 31, 34, 41, 9]



