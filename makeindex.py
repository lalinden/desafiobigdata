__author__ = 'Leonardo Linden'
__maintainer__ = 'Leonardo Augusto Linden'
__copyright__  = 'Exemplo'
__credits__ = ['Leonardo Augusto Linden']
__license__ = 'FreeL'
__version__ = '1.0'
__email__   = 'lalinden@gmail.com'
__status__  = 'Development'

from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import *
from pyspark.sql.functions import *
from pyspark.sql import SparkSession, functions as F
from pyspark import SparkConf
from pyspark.sql import SparkSession
import argparse, re, os

#-----------------------------------------------------------------------------------------------------------------------------------------
#
#  Sao gerados 3 arquivos .txt
#  index.txt - indice desejado (word, ID)
#  dict.txt - dicionario (ID, files)
#  map_word_index.txt - mapa para verificacao/conferencia do word/ID/files
#
#  Exemplo a palavra 'Fixedly' aparece como segue nos 3 arquivos:
#
#  index.txt
#  Fixedly                   14
#
#  dict.txt
#  (14, [11, 13, 15, 18, 2, 26, 30, 31, 34, 41, 9])
#
#  map_word_index.txt
#  Fixedly                   (14) - [11, 13, 15, 18, 2, 26, 30, 31, 34, 41, 9]
#
#
#-----------------------------------------------------------------------------------------------------------------------------------------


# poderia deixar apenas o split(), mas fiz aqui um clean basico para evitar palavras sem sentido, mais como exemplo do tratamento
def wordSplit(line):
    ignoreList = ['in', 'out', 'the', 'is', 'me', 'he', 'im', 'mr']     # exemplo de lista de palavras a ignorar
    linefix = fixLineChars(line)
    wordList = linefix.split()
    # remove palavras invalidas ou indesejadas
    cleanWords = []
    for word in wordList:
        word = word.strip().lower()
        if word not in ignoreList:
            if len(word) >= 3:               # ignora palavras com menos de 3 caracteres
                if not word.isdigit():
                    word = word.title()
                    cleanWords.append( word )
    return cleanWords

# remove tags html
def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

# exemplo de sanitizacao da linha a ser feito o split, redundacia de opcoes para mostrar a possivel combinacao das mesmas
def fixLineChars(lineparam):
    line = cleanhtml(lineparam)
    p2 = re.compile(r'[ ]{2,20}', re.VERBOSE)
    fixed = p2.sub(' ', line)
    p3 = re.compile(r'[\.]{2,20}', re.VERBOSE)
    fixed = p2.sub(' ', fixed)
    fixed = fixed.replace('&',' ').replace('/',' ').replace('"',' ').replace('\'',' ')
    fixed = re.sub('[^0-9a-zA-Z ]+', ' ', fixed)
    return fixed

# (u'sunbeams', (u'file:/home/lalinden/webmotors/dataset/41', 2, u'file:/home/lalinden/webmotors/dataset/1', 1, u'file:/home/lalinden/webmotors/dataset/43', 2))
def fileList( item ):
    filesNames = []
    for y in item:
        fp = y[0].split('/')
        last = len(fp)-1
        fna = fp[last]
        if fna not in filesNames:
            filesNames.append(fna)
    filesNames.sort()
    return ', '.join(filesNames)

def fileListStr( param ):
    word = param[ 0 ]
    item = param[ 1 ]
    filesNames = fileList( item )
    fnames = ', '.join(filesNames)
    s = '(%s, [%s])' % (word, fnames)
    return s

def fileListStrIndexed( param, idx ):
    word = param[ 0 ]
    item = param[ 1 ]
    filesNames = fileList( item )
    fnames = ', '.join(filesNames)
    s = '(%s %s)--(%s, [%s])---(%s, [%s])' % (word.ljust(15), idx,  word, fnames, idx, fnames)
    return s

def quiet_logs( sc ):
    logger = sc._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel( logger.Level.ERROR )
    logger.LogManager.getLogger("akka").setLevel( logger.Level.ERROR )
    return None

#---------------------------------------------------------------------------------------------------------------------------------
def buildIndexMap(input_path, out_path):
    global g_accumulator

    sc = SparkContext(conf=SparkConf())
    sc.setLogLevel("ERROR")     # ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE or WARN
    spark = SparkSession(sc).builder.master("local[*]").appName("index_builder").getOrCreate()
    quiet_logs( sc )

    # mapa inicial de todos os arquivos com WORD => file list
    rdd1 = sc.wholeTextFiles(input_path) \
            .flatMap(lambda (name, content): map(lambda word: (word, name), wordSplit(content) )) \
            .map(lambda (word, name): ((word, name), 1)) \
            .reduceByKey(lambda count1, count2: count1 + count2) \
            .map(lambda ((word, name), count): (word, (name, count)) ) \
            .groupByKey()
    rdd1.persist() #pyspark.StorageLevel.MEMORY_AND_DISK)

    rdd_1 = rdd1.map(lambda x : (x[0], fileList( x[1] ) ))             
    print('--------------- RDD Base ---------------------')
    # for line in rdd_1.takeOrdered(100, lambda s:1*s):
    #     print(line)
    df1 = spark.createDataFrame(rdd_1.collect())
    df1.show(truncate=False)

    # aqui pode ser pensada outra implementacao, como com Accmulator() para caso de rodar em cluster e nao apenas local
    def get_idx( x ):
        global g_accumulator
        g_accumulator = g_accumulator + x
        return str(g_accumulator)

    def makeIndexFilesPair( fObj ):
        pair = []
        pair.append( get_idx(1) )        
        pair.append( '[%s]' % fileList(fObj) )
        return pair

    #--- cria o indice para cada palavra agrupando com a lista de aqruivos para desmembrar em seguida 
    rdd_2 = rdd1.map(lambda x : (x[0], list( makeIndexFilesPair( x[1] ) ) ))             
    rdd_2.persist()
    print('---- RDD Index Mapping:  | word | [index, [file1, file2, ...]] | ')
    df2 = spark.createDataFrame(rdd_2.collect())
    df2.show(truncate=False)

    mfile = open( out_path + '/map_word_index.txt', "w") 
    for line in rdd_2.takeOrdered(99999999, lambda s:1*s):
        s = '%s (%s) - %s\r\n' % ( line[0].ljust(25), line[1][0], line[1][1] )
        mfile.write( s ) 
    mfile.close() 

    def getIndex( x, pos ):
        return x[ pos ]

    #--- indice, composto da palavra e do ID gerado
    rdd_3 = rdd_2.map( lambda x : ( x[0], getIndex(x[1], 0) ) )             
    print('--------------- RDD Index ---------------------')
    df3 = spark.createDataFrame(rdd_3.collect())
    df3.show(truncate=False)
    df3.coalesce(1).write.format("com.databricks.spark.csv").option("header", "false").mode('overwrite').save(out_path + '/index.csv')

    #--- gera o txt do indice formatado
    ifile = open( out_path + '/index.txt', "w") 
    for line in rdd_3.takeOrdered(99999999, lambda s:1*s):
        s = '%s %s\r\n' % (line[0].ljust(25), line[1])
        ifile.write( s ) 
    ifile.close() 

    #--- dicionario, composto do ID e da lista de arquivos relativos a palavra do indice
    rdd_4 = rdd_2.map( lambda x : ( getIndex(x[1], 0), getIndex( x[1], 1) ) )             
    print('--------------- RDD Dictionary ---------------------')
    df4 = spark.createDataFrame(rdd_4.collect())
    df4.show(truncate=False)
    df4.coalesce(1).write.format("com.databricks.spark.csv").option("header", "false").mode('overwrite').save(out_path + '/dict.csv')

    #--- gera o txt do dicionario, composto pelo ID e a lista de arquivos da palavra
    dfile = open( out_path + '/dict.txt', "w") 
    for line in rdd_4.takeOrdered(99999999, lambda s:1*s):
        s = '(%s, %s)\r\n' % (line[0], line[1])
        dfile.write( s ) 
    dfile.close() 

    sc.stop()
    return None


g_accumulator = 0

if __name__ == "__main__":
    curPath = os.path.dirname(os.path.abspath(__file__))
    dataPath = curPath + '/dataset'
    outPath = curPath + '/out'    
    if not os.path.exists(outPath):
        os.makedirs(outPath)    

    buildIndexMap(dataPath, outPath)
    print('finalizdo!')
